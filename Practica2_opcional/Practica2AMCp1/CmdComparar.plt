set title " Comparacion tiempos dyv y n2"
set key top left vertical inside 
set grid
set xlabel "Ciudades"
set ylabel "Tiempo (ms)"
plot "dyv.dat" using 1:2  with lines title"dyv","n2.dat" using 1:2 with lines title"n2"
set terminal pdf
set output"dyvn2.pdf"
replot
pause 5 "Pulsa Enter para continuar..."