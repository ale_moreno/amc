/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2amcp1;

import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Victor
 */
public class Graficas {
    
    public void generarGrafiaMEDIO(String nombre)
    {
        
        try
        {
                 FileWriter f1=new FileWriter("CmdMedio.plt");
                 PrintWriter fichero = new PrintWriter(f1);
     fichero.write("set title " +"\""+nombre + "\"\n");
     fichero.write("set key top left vertical inside\n");
     
     fichero.write("set grid\n");
	fichero.write("set xlabel "+ "\"Talla (n)\"\n");
	fichero.write("set ylabel " + "\"Tiempo (ms)\"\n");
	fichero.write("plot \"" + nombre + ".dat\" using 1:2  with lines title\"" + nombre + "\n");
	fichero.write("set terminal pdf\n");
	fichero.write("set output\"" + nombre +".pdf\"\n");
	fichero.write("replot\n");
	fichero.write("pause 5 \"Pulsa Enter para continuar...\"");
        fichero.close();
        Runtime.getRuntime().exec("Gnuplot CmdMedio.plt");
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
    
    public void generarGraficaCMP(String nombre1, String nombre2)
    {
       
        try
        {
        FileWriter f1=new FileWriter("CmdComparar.plt");
        PrintWriter fichero = new PrintWriter(f1);
     fichero.write("set title \" Comparacion tiempos " + nombre1 + " y " + nombre2 + "\"\n");
     fichero.write("set key top left vertical inside \n");     
     fichero.write("set grid\n");
	fichero.write("set xlabel "+ "\"Ciudades\"\n");
	fichero.write("set ylabel \"Tiempo (ms)\"\n");
	fichero.write("plot \"" + nombre1 + ".dat\" using 1:2  with lines title\"" + nombre1 + "\",\"" + nombre2 + ".dat" + "\" using 1:2 with lines title\"" + nombre2 + "\"\n");
	fichero.write("set terminal pdf\n");
	fichero.write("set output\"" + nombre1 + nombre2 + ".pdf\"\n");
	fichero.write("replot\n");
	fichero.write("pause 5 \"Pulsa Enter para continuar...\"");
        fichero.close();
        //Runtime.getRuntime().exec("gnuplot CmdComparar.plt");
    }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}
