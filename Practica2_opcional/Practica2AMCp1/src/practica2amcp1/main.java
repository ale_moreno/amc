package practica2amcp1;

import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author Victor
 */
public class main {

    public static void main(String[] args) throws IOException {
        punto[] P = new punto[50];
        double matriz[][] = new double[50][50];

        int opcion, opcion2, cap = 0, tama = 0;
        double minimo, min = 0;
        int position[];
        String cadena;
        Fichero f1 = new Fichero();
        Scanner teclado = new Scanner(System.in);
        Scanner t2 = new Scanner(System.in);

        Scanner t3 = new Scanner(System.in);

        boolean error = false;
        Test t1 = new Test();
        double min1;
        String[] arrcad = new String[5];
        arrcad[0] = "berlin52.tsp";
        arrcad[1] = "ch130.tsp";
        arrcad[2] = "ch150.tsp";
        arrcad[3] = "d493.tsp";
        arrcad[4] = "d657.tsp";

        int[] size = new int[5];

        Algoritmos dv1 = new Algoritmos();

        do {
            System.out.println("***************** MENU PRINCIPAL ****************"
                    + "\n\n1.- DIVIDE Y VENCERAS\n "
                    + "2.- PUNTOS CERCANOS N2 \n"
                    + "3.- VIAJANTE VORAZ\n"
                    + "4.- VIAJANTE EXHAUSTIVO\n"
                    + "5.- GRAFICA PARTE 1\n"
                    + "6.- GRAFICA PARTE 2\n"
                    + "0.- SALIR\n"
                    + "Eliga una opcion: ");

            opcion2 = teclado.nextInt();
            switch (opcion2) {
                case 0:
                    System.out.println("BYE BYE :)");
                    break;
                case 1:
                    do {
                        size[0] = 52;
                        size[1] = 130;
                        size[2] = 150;
                        size[3] = 493;
                        size[4] = 657;
                        System.out.println("\n\n**************** MENU DIVIDE Y VENCERAS ****************"
                                + "\n1. - Berlin"
                                + "\n2. - ch130"
                                + "\n3. - ch150"
                                + "\n4. - d493"
                                + "\n5. - d657"
                                + "\n6. - Aleatorio"
                                + "\n7. - Leer Fichero"
                                + "\n0. - VOLVER"
                                + "\n\nEliga una opcion: ");
                        opcion = teclado.nextInt();

                        switch (opcion) {
                            case 0:
                                break;
                            case 1:
                                P = new punto[size[0]];
                                cadena = "berlin52.tsp";
                                f1.lecturafichero(P, cadena);
                                dv1.Quicksort(P, f1.getcap());
                                dv1.setmin(P);
                                min = dv1.calculardistancia(0, P.length - 1, P);
                                System.out.println("LA DISTANCIA MINIMA ES: " + min);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 2:
                                P = new punto[size[1]];
                                cadena = "ch130.tsp";
                                f1.lecturafichero(P, cadena);
                                dv1.Quicksort(P, f1.getcap());
                                dv1.setmin(P);
                                min = dv1.calculardistancia(0, P.length - 1, P);
                                System.out.println("LA DISTANCIA MINIMA ES: " + min);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 3:
                                P = new punto[size[2]];
                                cadena = "ch150.tsp";
                                f1.lecturafichero(P, cadena);
                                dv1.Quicksort(P, f1.getcap());
                                dv1.setmin(P);
                                min = dv1.calculardistancia(0, P.length - 1, P);
                                System.out.println("LA DISTANCIA MINIMA ES: " + min);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 4:
                                P = new punto[size[3]];
                                cadena = "d493.tsp";
                                f1.lecturafichero(P, cadena);
                                dv1.Quicksort(P, f1.getcap());
                                dv1.setmin(P);
                                min = dv1.calculardistancia(0, P.length - 1, P);
                                System.out.println("LA DISTANCIA MINIMA ES: " + min);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 5:
                                P = new punto[size[4]];
                                cadena = "d657.tsp";
                                f1.lecturafichero(P, cadena);
                                dv1.Quicksort(P, f1.getcap());
                                dv1.setmin(P);
                                min = dv1.calculardistancia(0, P.length - 1, P);
                                System.out.println("LA DISTANCIA MINIMA ES: " + min);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 6:
                                System.out.println("Introduzca un tamano: ");
                                int tam = teclado.nextInt();
                                P = new punto[tam];
                                P = dv1.aleatorio(tam);
                                System.out.println("\n\nNombre para el fichero: ");
                                cadena = t3.nextLine();
                                f1.crearficheroAutomatico(cadena, P);
                                dv1.Quicksort(P, tam);
                                dv1.setmin(P);
                                min = dv1.calculardistancia(0, P.length - 1, P);
                                System.out.println("DISTANCIA MINIMA: " + min);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());

                                break;
                            case 7:
                                System.out.println("Introduzca el nombre del fichero:");
                                cadena = t2.nextLine();
                                P = f1.crearfichero(cadena, P);
                                dv1.setc1(0);
                                dv1.setc2(P.length - 1);
                                dv1.Quicksort(P, P.length);
                                dv1.setmin(P);
                                min = dv1.calculardistancia(0, P.length - 1, P);
                                System.out.println("DISTANCIA MINIMA: " + min);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            default:
                                System.out.println("Opcion Incorrecta");
                                break;
                        }
                    } while (opcion != 0);

                    break;

                case 2:
                    do {
                        System.out.println("\n\n**************** MENU N2  ****************"
                                + "\n1. - Berlin"
                                + "\n2. - ch130"
                                + "\n3. - ch150"
                                + "\n4. - d493"
                                + "\n5. - d657"
                                + "\n6. - Aleatorio"
                                + "\n7. - Generar Fichero"
                                + "\n0. - VOLVER"
                                + "\n\nEliga una opcion: ");
                        opcion = teclado.nextInt();
                        size[0] = 52;
                        size[1] = 130;
                        size[2] = 150;
                        size[3] = 493;
                        size[4] = 657;
                        //opcion=2; 
                        switch (opcion) {
                            case 0:
                                break;
                            case 1:

                                cadena = "berlin52.tsp";
                                P = new punto[size[0]];
                                f1.lecturafichero(P, cadena);
                                dv1.setc1(1);
                                dv1.setc2(P.length);
                                min1 = dv1.distancia(P[0], P[P.length - 1]);
                                min1 = dv1.sinorden(P, 0, min1, P.length);
                                System.out.println("DISTANCIA MINIMA: " + min1);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 2:
                                cadena = "ch130.tsp";
                                P = new punto[size[1]];
                                f1.lecturafichero(P, cadena);
                                dv1.setc1(1);
                                dv1.setc2(P.length);
                                min1 = dv1.distancia(P[0], P[size[1] - 1]);
                                min1 = dv1.sinorden(P, 0, min1, size[1]);
                                System.out.println("DISTANCIA MINIMA: " + min1);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 3:
                                cadena = "ch150.tsp";
                                P = new punto[size[2]];
                                f1.lecturafichero(P, cadena);
                                dv1.setc1(1);
                                dv1.setc2(P.length);
                                min1 = dv1.distancia(P[0], P[size[2] - 1]);
                                min1 = dv1.sinorden(P, 0, min1, size[2]);
                                System.out.println("DISTANCIA MINIMA: " + min1);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 4:
                                cadena = "d493.tsp";
                                P = new punto[size[3]];
                                f1.lecturafichero(P, cadena);
                                dv1.setc1(1);
                                dv1.setc2(P.length);
                                min1 = dv1.distancia(P[0], P[size[3] - 1]);
                                min1 = dv1.sinorden(P, 0, min1, size[3]);
                                System.out.println("DISTANCIA MINIMA: " + min1);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 5:
                                cadena = "d657.tsp";
                                P = new punto[size[4]];
                                f1.lecturafichero(P, cadena);
                                dv1.setc1(1);
                                dv1.setc2(P.length);
                                min1 = dv1.distancia(P[0], P[size[4] - 1]);
                                min1 = dv1.sinorden(P, 0, min1, size[4]);
                                System.out.println("DISTANCIA MINIMA: " + min1);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            case 6:
                                System.out.println("Introduzca un tamano: ");
                                int tam = teclado.nextInt();
                                P = new punto[tam];
                                P = dv1.aleatorio(tam);
                                System.out.println("\n\nNombre para el fichero: ");
                                cadena = t3.nextLine();
                                f1.crearficheroAutomatico(cadena, P);
                                dv1.setc1(0);
                                dv1.setc2(P.length - 1);
                                min1 = dv1.distancia(P[0], P[tam - 1]);
                                min1 = dv1.sinorden(P, 0, min1, tam);
                                System.out.println("DISTANCIA MINIMA: " + min1);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());

                                break;
                            case 7:
                                System.out.println("Introduzca el nombre del fichero:");
                                cadena = t2.nextLine();
                                P = f1.crearfichero(cadena, P);
                                dv1.setc1(1);
                                dv1.setc2(P.length);
                                min1 = dv1.distancia(P[0], P[P.length - 1]);
                                min1 = dv1.sinorden(P, 0, min1, P.length);
                                System.out.println("DISTANCIA MINIMA: " + min1);
                                System.out.println("CIUDAD ORIGEN: " + dv1.getc1());
                                System.out.println("CIUDAD DESTINO: " + dv1.getc2());
                                break;
                            default:
                                System.out.println("Opcion Incorrecta");
                                break;
                        }

                    } while (opcion != 0);

                    break;
                case 3:

                    do {
                        System.out.println("\n\n**************** MENU VORAZ ****************"
                                + "\n1. - Berlin"
                                + "\n2. - ch130"
                                + "\n3. - ch150"
                                + "\n4. - d493"
                                + "\n5. - d657"
                                + "\n6. - Aleatorio"
                                + "\n7. - Leer Fichero"
                                + "\n0. - VOLVER"
                                + "\n\nEliga una opcion: ");
                        opcion = teclado.nextInt();
                        size[0] = 52;
                        size[1] = 130;
                        size[2] = 150;
                        size[3] = 493;
                        size[4] = 657;

                        switch (opcion) {
                            case 0:
                                break;
                            case 1:

                                cadena = "berlin52.tsp";
                                P = new punto[size[0]];

                                f1.lecturafichero(P, cadena);
                                matriz = dv1.matrizcostes(P, f1.getcap());
                                position = new int[f1.getcap() + 1];

                                minimo = dv1.viajanteVoraz(P, matriz, f1.getcap(), 0);
                                for (int l = 0; l < f1.getcap(); l++) {
                                    P[l].setestado(false);
                                }
                                for (int k = 0; k < f1.getcap(); k++) {

                                    min = dv1.viajanteVoraz(P, matriz, f1.getcap(), k);
                                    if (minimo > min) {
                                        minimo = min;
                                        for (int i = 0; i < f1.getcap() + 1; i++) {
                                            position[i] = dv1.posiciones[i];
                                        }

                                    }
                                    for (int l = 0; l < f1.getcap(); l++) {
                                        P[l].setestado(false);
                                    }

                                }
                                dv1.posiciones[f1.getcap() - 1] = dv1.prin + 1;
                                dv1.posiciones[f1.getcap()] = dv1.posiciones[0];
                                dv1.mostrarcamino(minimo,cadena);
                                System.out.println(minimo + ": MINIMO TOTAL");

                                break;
                            case 2:
                                cadena = "ch130.tsp";
                                P = new punto[size[1]];
                                f1.lecturafichero(P, cadena);
                                position = new int[f1.getcap() + 1];

                                matriz = dv1.matrizcostes(P, f1.getcap());
                                minimo = dv1.viajanteVoraz(P, matriz, f1.getcap(), 0);
                                for (int l = 0; l < f1.getcap(); l++) {
                                    P[l].setestado(false);
                                }
                                for (int k = 0; k < f1.getcap(); k++) {

                                    min = dv1.viajanteVoraz(P, matriz, f1.getcap(), k);
                                    if (minimo > min) {
                                        minimo = min;

                                    }
                                    for (int l = 0; l < f1.getcap(); l++) {
                                        P[l].setestado(false);
                                    }

                                }

                                dv1.mostrarcamino(minimo,cadena);
                                System.out.println(minimo + ": MINIMO TOTAL");
                                break;
                            case 3:
                                cadena = "ch150.tsp";
                                P = new punto[size[2]];

                                f1.lecturafichero(P, cadena);

                                position = new int[f1.getcap() + 1];
                                matriz = dv1.matrizcostes(P, f1.getcap());

                                minimo = dv1.viajanteVoraz(P, matriz, f1.getcap(), 0);
                                for (int l = 0; l < f1.getcap(); l++) {
                                    P[l].setestado(false);
                                }
                                for (int k = 0; k < f1.getcap(); k++) {

                                    min = dv1.viajanteVoraz(P, matriz, f1.getcap(), k);
                                    if (minimo > min) {
                                        minimo = min;

                                    }
                                    for (int l = 0; l < f1.getcap(); l++) {
                                        P[l].setestado(false);
                                    }

                                }

                                dv1.mostrarcamino(minimo,cadena);
                                System.out.println(minimo + ": MINIMO TOTAL");

                                break;
                            case 4:
                                cadena = "d493.tsp";
                                P = new punto[size[3]];
                                f1.lecturafichero(P, cadena);

                                position = new int[f1.getcap() + 1];
                                matriz = dv1.matrizcostes(P, f1.getcap());

                                minimo = dv1.viajanteVoraz(P, matriz, f1.getcap(), 0);
                                for (int l = 0; l < f1.getcap(); l++) {
                                    P[l].setestado(false);
                                }
                                for (int k = 0; k < f1.getcap(); k++) {

                                    min = dv1.viajanteVoraz(P, matriz, f1.getcap(), k);
                                    if (minimo > min) {
                                        minimo = min;

                                    }
                                    for (int l = 0; l < f1.getcap(); l++) {
                                        P[l].setestado(false);
                                    }

                                }

                                dv1.mostrarcamino(minimo,cadena);
                                System.out.println(minimo + ": MINIMO TOTAL");

                                break;
                            case 5:
                                cadena = "d657.tsp";
                                P = new punto[size[4]];
                                f1.lecturafichero(P, cadena);

                                position = new int[f1.getcap() + 1];
                                matriz = dv1.matrizcostes(P, f1.getcap());

                                minimo = dv1.viajanteVoraz(P, matriz, f1.getcap(), 0);
                                for (int l = 0; l < f1.getcap(); l++) {
                                    P[l].setestado(false);
                                }
                                for (int k = 0; k < f1.getcap(); k++) {

                                    min = dv1.viajanteVoraz(P, matriz, f1.getcap(), k);
                                    if (minimo > min) {
                                        minimo = min;

                                    }
                                    for (int l = 0; l < f1.getcap(); l++) {
                                        P[l].setestado(false);
                                    }

                                }

                                dv1.mostrarcamino(minimo,cadena);
                                System.out.println(minimo + ": MINIMO TOTAL");

                                break;
                            case 6:

                                System.out.println("Introduzca un tamano: ");
                                int tam = teclado.nextInt();
                                P = new punto[tam];
                                P = dv1.aleatorio(tam);
                                System.out.println("\n\nNombre para el fichero: ");
                                cadena = t3.nextLine();
                                f1.crearficheroAutomatico(cadena, P);
                                matriz = dv1.matrizcostes(P, tam);

                                position = new int[tam + 1];
                                minimo = dv1.viajanteVoraz(P, matriz, tam, 0);
                                for (int l = 0; l < tam; l++) {
                                    P[l].setestado(false);
                                }
                                for (int k = 0; k < tam; k++) {

                                    min = dv1.viajanteVoraz(P, matriz, tam, k);
                                    if (minimo > min) {
                                        minimo = min;

                                        for (int i = 0; i < tam + 1; i++) {
                                            position[i] = dv1.posiciones[i];
                                        }
                                    }
                                    for (int l = 0; l < tam; l++) {
                                        P[l].setestado(false);
                                    }

                                }
                                dv1.setposiciones(position);
                                dv1.mostrarcamino(minimo,cadena);
                                System.out.println(minimo + ": MINIMO TOTAL");
                                break;
                            case 7:
                                tama = 0;
                                error = false;
                                Scanner tecla = new Scanner(System.in);
                                System.out.println("Que fichero desea leer? ");
                                cadena = tecla.nextLine();
                                if (f1.existeFichero(cadena)) {
                                    tama = f1.getcapacidad(cadena + ".tsp");
                                    P = new punto[tama];
                                    f1.lecturafichero(P, cadena + ".tsp");

                                } else {
                                    System.out.println("EL FICHERO NO EXISTE, DEBERÁ CREARLO");
                                    P = f1.crearfichero(cadena, P);
                                }

                                if (!error) {
                                    matriz = dv1.matrizcostes(P, f1.getcap());
                                    position = new int[f1.getcap() + 1];

                                    minimo = dv1.viajanteVoraz(P, matriz, f1.getcap(), 0);
                                    for (int l = 0; l < f1.getcap(); l++) {
                                        P[l].setestado(false);
                                    }
                                    for (int k = 0; k < f1.getcap(); k++) {

                                        min = dv1.viajanteVoraz(P, matriz, f1.getcap(), k);
                                        if (minimo > min) {
                                            minimo = min;
                                            for (int i = 0; i < f1.getcap() + 1; i++) {
                                                position[i] = dv1.posiciones[i];
                                            }

                                        }
                                        for (int l = 0; l < f1.getcap(); l++) {
                                            P[l].setestado(false);
                                        }

                                    }

                                    dv1.setposiciones(position);
                                    dv1.mostrarcamino(minimo,cadena);
                                    System.out.println(minimo + ": MINIMO TOTAL");
                                }

                                break;

                            default:
                                System.out.println("Opcion Incorrecta");
                                break;
                        }

                    } while (opcion != 0);

                    break;

                case 4:
                    do {
                        System.out.println("\n\n**************** MENU EXHAUSTIVO ****************"
                                + "\n1.- LEER DESDE FICHERO"
                                + "\n0.- VOLVER");
                        opcion = teclado.nextInt();
                        switch (opcion) {
                            case 0:
                                break;
                            case 1:

                                tama = 0;
                                error = false;
                                Scanner tecla = new Scanner(System.in);
                                System.out.println("Que fichero desea leer? ");
                                cadena = tecla.nextLine();
                                if (f1.existeFichero(cadena)) {
                                    System.out.println("Cuantas lineas desea leer? ");
                                    tama = teclado.nextInt();
                                    P = new punto[tama];
                                    error = f1.lecturaficheroAcotada(P, cadena + ".tsp", tama);

                                } else {
                                    System.out.println("EL FICHERO NO EXISTE, DEBERÁ CREARLO");
                                    P = f1.crearfichero(cadena, P);
                                }

                                if (!error) {
                                    matriz = dv1.matrizcostes(P, P.length);
                                    int n = P.length - 1;                  //Tipos para escoger
                                    int r = P.length;   //Elementos elegidos
                                    int mini = 999999;
                                    for (int j = 0; j < P.length; j++) {
                                        P[j].setestado(true);
                                        dv1.viajanteExhaustivo(P, matriz, j, j, j + ", ", n, r, mini);     //   dv1.
                                        //   P[j].setestado(false);
                                    }
                                    dv1.mostrarcamino(dv1.gettotal2(),cadena);
                                    System.out.println("EL COSTE TOTAL ES: " + dv1.gettotal2());
                                }
                                break;
                            default:
                                System.out.println("\nOpcion Incorrecta\n\n");
                                break;
                        }
                    } while (opcion != 0);
                    break;
                case 5:
                    size[0] = 52;
                        size[1] = 130;
                        size[2] = 150;
                        size[3] = 493;
                        size[4] = 657;
                    t1.Graficaparte1(size);
                    break;
                case 6:
                    size[0]=6;
                    size[1]=7;
                    size[2]=8;
                    size[3]=9;
                    size[4]=10;
                    t1.Graficaparte2(size);
                    break;
                default:
                    System.out.println("Opcion incorrecta");
                    break;
            }
        } while (opcion2 != 0);

    }
}
