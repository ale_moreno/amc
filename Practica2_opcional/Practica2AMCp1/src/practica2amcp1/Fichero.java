/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2amcp1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Victor
 */
public class Fichero {
    private int cap=0;
    
    
    public int getcap(){return cap;}
    
    public int getcapacidad(String nombre) throws FileNotFoundException, IOException
    {
     FileReader f2=new FileReader(nombre);
        BufferedReader b = new BufferedReader(f2);
           String[] arraycadena = null;
String cadena2=new String();
                for (int i = 0; i < 6; i++) {
                    if(i==4)
                    {
                                        arraycadena = cadena2.split(" ");
                                        cap=Integer.parseInt(arraycadena[1]);
                                        
                    }
                    cadena2=b.readLine();
                }
                return cap;
            }
    public void lecturafichero(punto [] p, String cadena)
    {
        
        int j=0;
        cap=0;
        String[] arraycadena = null;
        String cadena2=new String();

        try {

       FileReader f=new FileReader(cadena);
        BufferedReader b = new BufferedReader(f);
            try {

                for (int i = 0; i < 6; i++) {
                    if(i==4)
                    {
                                        arraycadena = cadena2.split(" ");
                                        cap=Integer.parseInt(arraycadena[1]);
                                        
                    }
                    cadena2=b.readLine();
                    
                }
                
                            
                while(j<cap)
                {
                    cadena2 = b.readLine();
                     arraycadena = cadena2.split(" ");
                     double x=   Double.parseDouble(arraycadena[1]);
                     double y= Double.parseDouble(arraycadena[2]);
//                     System.out.println(x + " , " + y);
                     //p.add(new punto(x,y));  
                        p[j]=new punto(x,y,j);
                    
                        
                j++;
                }
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                b.close();
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean existeFichero(String cadena)
    {
        File f1=new File(cadena+".tsp");
        return f1.exists();
    }
    
    public boolean lecturaficheroAcotada(punto [] p, String cadena, int capa)
    {
        boolean error=false;
        int j=0;
       int tama=0;
        String[] arraycadena = null;
        String cadena2=new String();

        try {

       FileReader f=new FileReader(cadena);
        BufferedReader b = new BufferedReader(f);
            try {

                for (int i = 0; i < 6; i++) {
                    if(i==4)
                    {
                        arraycadena = cadena2.split(" ");
                                        tama=Integer.parseInt(arraycadena[1]);
                                        
                    }
                                    cadena2=b.readLine();
                    
                }
                
                   if(tama<capa)
                   {
                       System.out.println("EL FICHERO TIENE UN TAMANO DE: " + tama + "\nPOR LO QUE NO PODRIAMOS LEER TANTAS LINEAS");
                       error=true;
                   }
                   else
                   {
                while(j<capa)
                {
                    cadena2 = b.readLine();
                     arraycadena = cadena2.split(" ");
                     double x=   Double.parseDouble(arraycadena[1]);
                     double y= Double.parseDouble(arraycadena[2]);
//                     System.out.println(x + " , " + y);
                     //p.add(new punto(x,y));  
                        p[j]=new punto(x,y,j);
                    
                        
                j++;
                }
                   }    } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                b.close();
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return error;
    }
    
    
       public punto[] crearfichero(String nombre, punto [] p) throws IOException

//    public punto [] crearfichero(String nombre, punto [] p) throws IOException
    {
           Double x,y;
           int capacidad;
        File f1=new File(nombre+".tsp");
        
        
               
                   Scanner teclado=new Scanner(System.in);
        if(f1.exists())
        {
            System.out.println("ESTE FICHERO YA EXISTE");
            capacidad=getcapacidad(nombre+".tsp");
            
            p=new punto[capacidad];
            
            //System.out.println("CAPACIDAD DE P: " + p.length);
            
        }
       else
        {
        
        FileWriter f=new FileWriter(nombre+".tsp");
                   PrintWriter fichero = new PrintWriter(f);
        System.out.println("Cuantas ciudades desea crear? ");
        capacidad=teclado.nextInt();
        
        p=new punto[capacidad];
          
        
        fichero.write("NAME: " + nombre + "\n");
        fichero.write("TYPE: TSP\n");
        fichero.write("COMMENT: " + capacidad + " locations in " + nombre + "\n" );
        fichero.write("DIMENSION: " + capacidad + "\n");
        fichero.write("EDGE_WEIGHT_TYPE: EUC_2D\n" );
        fichero.write("NODE_COORD_SECTION\n");
             
        for (int i = 0; i < capacidad; i++) {
            System.out.println("CIUDAD " +(i+1)+ ": Coordenada X: "            );
           x=teclado.nextDouble();
            System.out.println("Coordenada Y: ");
            y=teclado.nextDouble();
            fichero.write((i+1) + " " + x + " " + y + "\n" );
        }
        fichero.write("EOF");
            fichero.close();
        }
          
            
       int j=0;
        cap=0;
        String[] arraycadena = null;
        String cadena2=new String();

        try {

       FileReader f2=new FileReader(nombre + ".tsp");
        BufferedReader b = new BufferedReader(f2);
            try {

                for (int i = 0; i < 6; i++) {
                    if(i==4)
                    {
                                        arraycadena = cadena2.split(" ");
                                        cap=Integer.parseInt(arraycadena[1]);
                                        
                    }
                    cadena2=b.readLine();
                    
                }
                
                            
                while(j<cap)
                {
                    cadena2 = b.readLine();
                     arraycadena = cadena2.split(" ");
                     x=   Double.parseDouble(arraycadena[1]);
                     y= Double.parseDouble(arraycadena[2]);
//                     System.out.println(x + " , " + y);
                     //p.add(new punto(x,y));  
                        p[j]=new punto(x,y,j);
                    
                        
                j++;
                }
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                b.close();
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }     
            return p;
      
      
       }
       
       
       
        public punto[] crearficheroAutomatico(String nombre, punto [] p) throws IOException

//    public punto [] crearfichero(String nombre, punto [] p) throws IOException
    {
           Double x,y;
           int capacidad;
        File f1=new File(nombre+".tsp");
        
        
               
                   Scanner teclado=new Scanner(System.in);
        if(f1.exists())
        {
            System.out.println("ESTE FICHERO YA EXISTE");
            capacidad=getcapacidad(nombre+".tsp");
            
            p=new punto[capacidad];
            
            //System.out.println("CAPACIDAD DE P: " + p.length);
            
        }
       else
        {
        
        FileWriter f=new FileWriter(nombre+".tsp");
                   PrintWriter fichero = new PrintWriter(f);
        //System.out.println("Cuantas ciudades desea crear? ");
        //capacidad=teclado.nextInt();
        
       // p=new punto[capacidad];
          capacidad=p.length;
        
        fichero.write("NAME: " + nombre + "\n");
        fichero.write("TYPE: TSP\n");
        fichero.write("COMMENT: " + capacidad + " locations in " + nombre + "\n" );
        fichero.write("DIMENSION: " + capacidad + "\n");
        fichero.write("EDGE_WEIGHT_TYPE: EUC_2D\n" );
        fichero.write("NODE_COORD_SECTION\n");
             
        for (int i = 0; i < capacidad; i++) {
            fichero.write((i+1) + " " + p[i].getx() + " " + p[i].gety() + "\n" );
        }
        fichero.write("EOF");
            fichero.close();
        }
          
            
       int j=0;
        cap=0;
        String[] arraycadena = null;
        String cadena2=new String();

        try {

       FileReader f2=new FileReader(nombre + ".tsp");
        BufferedReader b = new BufferedReader(f2);
            try {

                for (int i = 0; i < 6; i++) {
                    if(i==4)
                    {
                                        arraycadena = cadena2.split(" ");
                                        cap=Integer.parseInt(arraycadena[1]);
                                        
                    }
                    cadena2=b.readLine();
                    
                }
                
                            
                while(j<cap)
                {
                    cadena2 = b.readLine();
                     arraycadena = cadena2.split(" ");
                     x=   Double.parseDouble(arraycadena[1]);
                     y= Double.parseDouble(arraycadena[2]);
//                     System.out.println(x + " , " + y);
                     //p.add(new punto(x,y));  
                        p[j]=new punto(x,y,j);
                    
                        
                j++;
                }
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                b.close();
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }     
            return p;
      
      
       }
           
        
        
    public void crearoutput(String nombre, int [] p, double min) throws IOException
   {
           int capacidad;
        
        
               
                   Scanner teclado=new Scanner(System.in);
        
        FileWriter f=new FileWriter(nombre+".opt.tour");
                   PrintWriter fichero = new PrintWriter(f);
          capacidad=p.length;
        
        fichero.write("NAME: " + nombre + "\n");
        fichero.write("TYPE: TOUR\n");
        fichero.write("COMMENT: Length " + min + "\n" );
        fichero.write("DIMENSION: " + (capacidad-1) + "\n");
        fichero.write("TOUR_SECTION\n");
             
        for (int i = 0; i < capacidad-1; i++) {
            fichero.write(p[i] + "\n" );
        }
        fichero.write(-1 + "\nEOF");
        
            fichero.close();
        }
          
            
        
      
       }
    

