/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2amcp1;

import java.io.IOException;
import static java.lang.Math.abs;
import static java.lang.Math.rint;
import static java.lang.Math.sqrt;
import java.util.Random;
import java.util.Stack;

/**
 *
 * @author Victor
 */
public class Algoritmos {

    private double minimo;
    private int total;
    private int ciudad1 = 0;
    private int ciudad2 = 0;
    int[] posiciones;
    private int total2 = 999999999;
    int prin=0;
    double min;
    public Algoritmos() {
        minimo = 0;
        total = 0;
    }
    
    public void setposiciones(int[] position)
    {
        for (int i = 0; i < position.length; i++) {
            posiciones[i]=position[i];
        }
    }
    public int gettotal2() {
        return total2;
    }

    public void mostrarcamino(double m1, String nombre) throws IOException {
        Fichero f1=new Fichero();
        f1.crearoutput(nombre, posiciones, m1);
        System.out.println("CIUDAD ORIGEN: " + posiciones[0]);
        for (int i = 1; i < posiciones.length; i++) {
            System.out.println("CIUDAD " + (i + 1) + ": " + posiciones[i]);
        }
    }

    public int getc1() {
        return ciudad1;
    }

    public void setc1(int c1) {
        ciudad1 = c1;
    }

    public void setc2(int c2) {
        ciudad2 = c2;
    }

    public int getc2() {
        return ciudad2;
    }

    public punto[] aleatorio(int tam) {
        punto[] aux = new punto[tam];
        Random r = new Random();

        for (int i = 0; i < tam; i++) {
            aux[i] = new punto(r.nextDouble() * 2000, r.nextDouble() * 2000, i);

        }
        return aux;
    }

    public double distancia(punto p1, punto p2) {

        return (sqrt(((p1.getx() - p2.getx()) * (p1.getx() - p2.getx())) + ((p1.gety() - p2.gety()) * (p1.gety() - p2.gety()))));

    }

   /* 
    * public void Intercambia(punto a[], int i, int j) {
        punto aux = new punto(a[i].getx(), a[i].gety(), a[i].getNum());
        
        a[i].setxy(a[j].getx(), a[j].gety());
        a[i].setnum(a[j].getNum());
        a[j].setxy(aux.getx(), aux.gety());
        a[j].setnum(aux.getNum());
    }

    public int mypartition(punto T[], int e, int d) {
        punto x = T[e];
        int i = e - 1;
        int j = d + 1;

        for (;;) {
            while (x.getx() < T[--j].getx());
            while (T[++i].getx() < x.getx());
            if (i >= j) {
                return j;
            }

            Intercambia(T, i, j);
        }

    }

    public void vervector(punto T[]) {
        System.out.println("VER VECTOR");
        for (int i = 0; i < 9; i++) {
            System.out.println("P[i]" + T[i].getx());
        }
    }

    public void quick_sort(punto T[], int e, int d) {
        if (e < d) {
            int q = mypartition(T, e, d);
            quick_sort(T, e, q);
            quick_sort(T, q + 1, d);
        }
    }

    public void Quicksort(punto T[], int size) {

        quick_sort(T, 0, size - 1);
    }
*/
    public double sinorden(punto[] P, int e, double min, int size) {

        for (int i = e; i < size; i++) {
            int j = 0;
            while (j < size) {
                if (i != j) {
                    //                              System.out.println("Posicion I y D: " + i + "," + j);
                    if (distancia(P[i], P[j]) < min) {
                        this.ciudad1 = P[i + 1].getNum();
                
                        this.ciudad2 = P[j + 1].getNum();

                        min = distancia(P[i], P[j]);
                        //                                System.out.println("Minimo entre " + i + "," + j + ": " + min);
                    }
                }
                j++;
            }
        }

        return min;
    }

    public void setmin(punto [] P )
    {
        min=distancia(P[0],P[P.length-1]);
    }
    public double calculardistancia(int iz, int de, punto [] P)
  {
     final int N= Math.abs(iz-de) + 1;
     if(Math.abs(iz-de)==0)
     {
         
         if(iz==P.length-1)
             return min;
         if(distancia(P[iz],P[de+1])<min)
         {
             min=distancia(P[iz],P[de+1]);
             ciudad1=P[iz].getNum()+1;
             ciudad2=P[de+1].getNum()+1;
             //ciudad1=iz;
             //ciudad2=de+1;
         }
     }
     else
     {
         calculardistancia(iz, (iz + de) / 2, P);
         calculardistancia(((iz + de) / 2) + 1, de, P);
         
     }
     return min;
  } 
    
   
    public int medio(punto T[], int medio, double minimo, char direccion) {
        boolean encontrado = false;
        int acum = 0;
        int i = 1;
        if (direccion == 'I') {
            acum += abs(T[medio].getx() - T[medio - 1].getx());
            while (!encontrado && i < medio) {

                if (acum < minimo) {
                    acum += abs(T[medio - i].getx() - T[medio - i - 1].getx());

                    i++;
                } else {
                    encontrado = true;
                }
            }
        } else {
            acum += abs(T[medio].getx() - T[medio + 1].getx());
            while (!encontrado && i < medio) {
                if (acum < minimo) {
                    acum += abs(T[medio + i].getx() - T[medio + i + 1].getx());

                    i++;
                } else {
                    encontrado = true;
                }
            }

        }

        return i;
    }

    

    public void comprobar(punto p[], int size, int inicio) {
        double min = 2000;
        int posicion = 0;
        for (int i = 0; i < size; i++) {
            if (inicio != i) {

                double xd = p[inicio].getx() - p[i].getx();
                double yd = p[inicio].gety() - p[i].gety();
                double minimo = rint(sqrt(xd * xd + yd * yd));
                System.out.println("Distancia entre " + inicio + "," + i + ": " + minimo);

                if (min > minimo) {
                    min = minimo;
                    posicion = i;

                }
            }

        }
        System.out.println("ESTE ES EL MINIMO: " + min
                + "\nPOSICION: " + posicion);
    }

    public double[][] matrizcostes(punto P[], int size) {
        double matriz[][] = new double[size][size];

        for (int i = 0; i < size; i++) {
            int j = i + 1;

            while (j < size) {
//                                  System.out.println("Posicion I y D: " + i + "," + j);
                double xd = abs(P[i].getx() - P[j].getx());
                double yd = abs(P[i].gety() - P[j].gety());

                matriz[i][j] = rint(sqrt(xd * xd + yd * yd));
//                                            System.out.println("MATRIZ["+i+"][" + j + "]: " + matriz[i][j]);
                //            matriz[i][j]=(int)distancia(P[i],P[j]);
                matriz[j][i] = matriz[i][j];
                j++;
            }
        }
//vermatriz(matriz,size);
        return matriz;
    }

    public void vermatriz(double matriz[][], int size) {
        for (int i = 0; i < size; i++) {
            for (int j = i + 1; j < size; j++) {
                System.out.println("M[" + i + "][" + j + "]:" + matriz[i][j]);
            }

        }
    }

 
    
    public double viajanteVoraz(punto[] P, double[][] matriz, int size, int inicio) {
        double total=0;
        int pos=0;
        boolean encontrado=false;
        double min;
        posiciones=new int[size+1];
        posiciones[0]=inicio;
       // System.out.println("NUEVO INICIO: " + inicio);
        
        
        for (int i = 0; i < size; i++) {
            P[inicio].setestado(true);
            min=99999999;
                for (int j = 0; j < size; j++) {
                if(!P[j].getestado())
                {
                    
                   // System.out.println("DISTANCIA CIUDAD " + inicio + " CON " + j + " TIENE UN MINIMO DE: " + matriz[inicio][j]);
                    if(matriz[inicio][j]<min)
                    {
                       // System.out.println("MINIMO " + matriz[inicio][j]);
                        min=matriz[inicio][j];
                        pos=j;
                    }
                }
            }
            
               total+=matriz[inicio][pos];
           inicio=pos; 
            posiciones[i+1]=inicio;
        
        }

        posiciones[size]=posiciones[0];

                total+=matriz[posiciones[size-1]][posiciones[size]];
         return total;
    }



    public void viajanteExhaustivo(punto[] elem, double[][] matriz, int inicio, int actual, String act, int n, int r, int mini) {//, int total) {

        for (int i = 0; i < r; i++) {
            if (i != inicio && i != actual) {
                elem[i].setestado(false);
            }
        }
        if (n == 0) {
            //act+=inicio;
            String[] arrayNum = act.split(", ");

            for (int i = 0; i < r - 1; i++) {           
                total += matriz[Integer.parseInt(arrayNum[i])][Integer.parseInt(arrayNum[i + 1])];
            }

            total += matriz[inicio][Integer.parseInt(arrayNum[r - 1])];
            if (total2 > total) {
                posiciones = new int[r+1];
                for (int i = 0; i < r; i++) {
                    posiciones[i] = Integer.parseInt(arrayNum[i]);
                }
                posiciones[r] = inicio;
               
                total2 = total;
            }

            total = 0;
        } else {

            for (int j = 0; j < r; j++) {
                if (j != inicio && !elem[j].getestado() && !act.contains(Integer.toString(j))) { // Controla que no haya repeticiones
                    elem[actual].setestado(true);

                    actual = j;

                    viajanteExhaustivo(elem, matriz, inicio, actual, act + j + ", ", n - 1, r, mini);//, total);

                }
            }

        }

    }

}
