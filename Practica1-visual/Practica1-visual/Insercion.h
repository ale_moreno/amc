#ifndef INSERCION_H
#define INSERCION_H

#include "Ordenacion.h"

using namespace std;
template <class T>
class Insercion : public Ordenacion<T>
{
    public:
        Insercion();
        ~Insercion();
        void ordena(T *vec, int ini, int fin);
};

#include "Insercion.cpp"

#endif // INSERCION_H
