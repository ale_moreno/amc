
template <class T>
Shellsort<T>::Shellsort(){}

template <class T>
Shellsort<T>::~Shellsort(){}

template <class T>
void Shellsort<T>::ordena(T *vec, int ini, int fin)
{
    for(int h = fin/2; h>0; h/=2)
    {
        for(int i = h; i<fin; i++)
        {
            int j = i;
            T eleOrde = vec[i];
            while(j>=h && eleOrde<vec[j-h])
            {
                vec[j] = vec[j-h];
                j -= h;
            }
            vec[j] = eleOrde;
        }
    }
}
