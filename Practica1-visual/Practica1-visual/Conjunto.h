#ifndef CONJUNTO_H
#define CONJUNTO_H

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <math.h>


// Clase conjunto de enteros con tama�o m�ximo limitado.
#define MAXIMO 7  // TA�A�O M�XIMO 5

using namespace std;

template <class T>
class Conjunto
{
    private:
        int tam;
        T* datos;
    public:
        Conjunto(int n);
        ~Conjunto();
        void ordena();
        void escribe();
        void clonar(const Conjunto &c);
        void generarAlea();
        void inverso();
        T* getVector();
};

#include "Conjunto.cpp"

#endif // CONJUNTOINT_H
