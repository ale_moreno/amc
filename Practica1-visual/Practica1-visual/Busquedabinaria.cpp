template <class T>
int Busbinaria<T>::busca(T* vec, int tama, int elem){

	Quicksort quick;

	quick.ordena(vec, 0, tama-1);
	if (vec[tama / 2] == elem)
		return tama / 2;
	else if (vec[tama / 2] < elem)
		return busca(vec, elem, 0, tam / 2 - 1);
	else
		return busca(vec, elem, tam / 2 + 1, tam-1);
}

template <class T>
int Busbinaria<T>::busca(T* vec, int elem, int ini, int fin){
	if (ini>fin) return -1;
	else if (ini == fin) return (vec[ini] == elem) ? ini : -1;
	else
	{
		int mitad = (fin - ini + 1) / 2;
		if (vec[mitad] == elem) return mitad;
		else if (vec[mitad] > elem)
			return busca(vec, elem, mitad + 1, fin);
		else
			return busca(vec, elem, ini, mitad - 1);
	}

}
