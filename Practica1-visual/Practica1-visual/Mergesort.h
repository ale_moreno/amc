#ifndef MERGESORT_H
#define MERGESORT_H

#include "Ordenacion.h"
#include <iostream>

using namespace std;
template <class T>
class Mergesort : public Ordenacion<T>
{
    private:
        void mezcla(T *vec, int ini, int corte, int fin);
        void mergeS(T *vec, int ini, int fin);
    public:
        Mergesort();
        ~Mergesort();
        void ordena(T *vec, int ini, int fin);


};

#include "Mergesort.cpp"

#endif // MERGESORT_H
