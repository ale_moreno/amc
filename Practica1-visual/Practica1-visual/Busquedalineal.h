#ifndef BLINEAL_H
#define BLINEAL_H


#include "Busquedas.h"

using namespace std;
template <class T>
class Buslineal :public Busqueda<T>
{
public:
	Buslineal();
	~Buslineal();
	int busca(T* vec, int tama, int elem);
};

#include "Busquedalineal.cpp"

#endif