#ifndef ORDENACION_H
#define ORDENACION_H

using namespace std;
template <class T>
class Ordenacion
{
    public:
        Ordenacion();
        virtual ~Ordenacion();
        virtual void ordena(T *vec, int ini, int fin)=0;
        void intercambia(T *vec, int pos1, int pos2);

};

#include "Ordenacion.cpp"

#endif // ORDENACION_H
