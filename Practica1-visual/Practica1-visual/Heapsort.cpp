template <class T>
Heapsort<T>::Heapsort(){}

template <class T>
Heapsort<T>::~Heapsort(){}

template <class T>
void Heapsort<T>::ordena(T* vec, int ini, int fin){
	build_heap(vec, ini, fin);
	for (int i = fin - 1; i >= 1; --i){
		this->intercambia(vec, 0, i);
		heapify(vec, i, 0);
	}
}

template <class T>
void Heapsort<T>::build_heap(T* vec, int ini, int fin){
	for (int i = fin / 2 - 1; i >= 0; i--)
		heapify(vec, fin, i);
}

template <class T>
void Heapsort<T>::heapify(T* vec, int fin, int i){
	T x = vec[i];
	int c = 2 * i + 1;
	while (c < fin){
		if (c + 1 < fin && vec[c] < vec[c + 1])
			c++;
		if (x >= vec[c])break;
		vec[i] = vec[c];
		i = c;
		c = 2 * i + 1;
	}
	vec[i] = x;
}