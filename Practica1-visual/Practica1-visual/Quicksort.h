#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "Ordenacion.h"

using namespace std;
template <class T>
class Quicksort : public Ordenacion<T>
{
    private:
        int particion(T *vec, int ini, int fin);
        void quick(T *vec, int ini, int fin);
    public:
        Quicksort();
        virtual ~Quicksort();
        void ordena(T *vec, int ini, int fin);

};

#include "Quicksort.cpp"

#endif // QUICKSORT_H
