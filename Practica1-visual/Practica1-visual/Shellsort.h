#ifndef SHELLSORT_H
#define SHELLSORT_H

#include "Ordenacion.h"

using namespace std;
template <class T>
class Shellsort : public Ordenacion<T>
{
    public:
        Shellsort();
        virtual ~Shellsort();
        void ordena(T *vec, int ini, int fin);
};

#include "Shellsort.cpp"

#endif // SHELLSORT_H
