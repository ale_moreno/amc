reset 
set title 'Comparacion Metodos' 
set key top left vertical inside
set xlabel 'Talla (n)' 
set ylabel 'Tiempo (milisegundos)' 
set grid xtics 
set grid ytics 
plot 'Heapsort.dat' w linespoints title 'Heapsort' 
replot 'Seleccion.dat' w linespoints title 'Seleccion' 
set terminal pdf
set output "Comparacion.pdf" 
replot
pause 5 'Pulsa Enter para continuar...'
