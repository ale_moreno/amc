template<class T>
int Buslieal<T>::busca(T *vec, int tama, int elem){
	int i = 0;
	while (i < tama && vec[i]!=elem){
		i++;
	}
	return (i < tama) ? i : -1;
}