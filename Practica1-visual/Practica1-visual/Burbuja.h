#ifndef BURBUJA_H
#define BURBUJA_H

#include "Ordenacion.h"

using namespace std;
template <class T>
class Burbuja : public Ordenacion<T>
{
    public:
        Burbuja();
        virtual ~Burbuja();
        void ordena(T *vec, int ini, int fin);
};

#include "Burbuja.cpp"

#endif // BURBUJA_H
