#ifndef HEAPSORT_H
#define HEAPSORT_H

#include "Ordenacion.h"

using namespace std;
template <class T>
class Heapsort : public Ordenacion<T>
{

	public:
		Heapsort();
		~Heapsort();
		void ordena(T* vec, int ini, int fin);
		void build_heap(T* vec, int ini, int fin);
		void heapify(T* vec, int fin, int i);
};

#include "Heapsort.cpp"
#endif; //HEAPSORT_H