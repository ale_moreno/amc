
template <class T>
Quicksort<T>::Quicksort(){}

template <class T>
Quicksort<T>::~Quicksort(){}

template <class T>
void Quicksort<T>::ordena(T *vec, int ini, int fin)
{
        quick(vec,ini,fin-1);
}

template <class T>
void Quicksort<T>::quick(T *vec, int ini, int fin)
{
    if(ini<fin)
    {
        int corte = particion(vec,ini,fin);
        quick(vec,ini,corte-1);
        quick(vec,corte+1,fin);
    }
}

template <class T>
int Quicksort<T>::particion(T *vec, int ini, int fin)
{
    T pivote = vec[ini];
    int i = ini,
        j = fin,
        turno = 0;

    while(i<j)
    {
        while (pivote<vec[j])
            j--;
        while (i<j && vec[i]<=pivote)
            i++;
        if (i<j)
            this->intercambia(vec, i, j);
    }
    this->intercambia(vec, ini, j);
    return j;
}
