#include <iostream>
#include "TestOrdenacion.h"


using namespace std;

int menumain(){
	int opc;
	cout << "\t\t\t\t\t\t\t\t\t Realizado por: Gonzalo Fernandez y Alejandro Moreno" << endl;
	cout << endl << endl << endl;
	cout << "\n\n********* MENU PRINCIPAL ********* \n" << endl;
	cout << "1.- Metodos de ordenacion" << endl;
	cout << "2.- Metodos de busqueda" << endl;
	cout << "0.- Salir" << endl;
	cout << "Elige una opcion: ";
	cin >> opc;
	return opc;
}

int menuPpal()
{
    int opc;
    cout<<"\n\n********* MENU ORDENACION/BUSQUEDA **********\n"<<endl;
    cout<<"1.- Probar los metodos"<<endl;
    cout<<"2.- Obtener el caso medio de un metodo"<<endl;
    cout<<"3.- Comparar metodos"<<endl;
    cout<<"0.- Salir"<<endl;
    cout<<"Elige una opcion: ";
    cin>>opc;

    return opc;
}

int elecionMetodoO()
{
    int opc;
    cout<<"1.- Burbuja"<<endl;
    cout<<"2.- Insercion"<<endl;
	cout << "3.- Seleccion" << endl;
    cout << "4.- ShellSort"<< endl;
    cout<<"5.- MergeSort"<<endl;
    cout<<"6.- QuickSort"<<endl;
	cout <<"7.- Heapsort"<< endl;
    cout<<"Elige una opcion: ";
    cin>>opc;

    return opc;
}

int eleccionMetodoB()
{
	int opc;
	cout << "1.- Busqueda lineal" << endl;
	cout << "2.- Busqueda binaria" << endl;
	cout << "3.- Busqueda por hash" << endl;
	cout << "Elige una opcion: ";
	cin >> opc;

	return opc;
}

int main()
{

    int mmain, opc, metodo, talla;
    vector<int> comp;
    TestOrdenacion<int> pruebas;
    //TestOrdenacion<double> pruebas;
	//TestOrdenacion<float> pruebas;
    //TestOrdenacion<char> pruebas;

	do{
		mmain = menumain();
		switch (mmain)
		{
		case 1:				//Ordenacion
			do
			{
				opc = menuPpal();
				switch (opc)
				{
				case 1:		//Probar metodo
					cout << "Nro de elementos en el vector: ";
					cin >> talla;
					pruebas.comprobarMetodos(talla);
					break;
				case 2:		//Obtener caso medio

					do
					{
						metodo = elecionMetodoO() - 1;
						if (metodo >= 1 && metodo <= 7)
						{
							comp.push_back(metodo);
						}
					} while (metodo<0 || metodo>5);
					pruebas.compararMetodos(comp);
					comp.clear();
					break;
				case 3:		//Comparar dos metodos
					do
					{
						metodo = elecionMetodoO();
						if (metodo >= 1 && metodo <= 7)
						{
							comp.push_back(metodo - 1);
						}
						if (metodo != 0)
							cout << "\n\n0.- Finalizar " << endl;
					} while (metodo != 0);
					pruebas.compararMetodos(comp);
					comp.clear();
					break;
				case 0:
					cout << "Adios!" << endl;
					break;
				default:
					cout << "Opcion incorrecta" << endl;
				}
			} while (opc != 0);
			break;
		case 2:				//Busqueda
			do
			{
				opc = menuPpal();
				switch (opc)
				{
				case 1:		//Probar metodos de busqueda
					cout << "Nro de elementos en el vector: ";
					cin >> talla;
							//Test de busqueda
					break;
				case 2:		//Obtener caso medio
					do
					{
						metodo = eleccionMetodoB() - 1;
						if (metodo >= 0 && metodo <= 3)
						{
							//comp.push_back(metodo);

						}
					} while (metodo<0 || metodo>3);
							//pruebas.compararMetodos(comp);
					comp.clear();
					break;
				case 3:		//Comparar dos metodos
					do
					{
						metodo = eleccionMetodoB();
						if (metodo >= 1 && metodo <= 3)
						{
							//comp.push_back(metodo - 1);

						}
						if (metodo != 0)
							cout << "\n\n0.- Finalizar " << endl;
					} while (metodo != 0);
							//pruebas.compararMetodos(comp);
					comp.clear();
					break;
				case 0:		//salir
					cout << "Adios!" << endl;
					break;
				default:
					cout << "Opcion incorrecta" << endl;
				}
			} while (opc != 0);
			break;
		case 0:
			cout << "Adios!" << endl;
			break;
		default:
			cout << "Opcion incorrecta" << endl;
		}
	} while (mmain != 0);
}
