template <class T>
Seleccion<T>::Seleccion(){}

template <class T>
Seleccion<T>::~Seleccion(){}

template <class T>
void Seleccion<T>::ordena(T* vec, int ini, int fin){
	
	for (int i = ini; i < fin; i++){
		int m = pos_min(vec, i, fin);
		this->intercambia(vec, i, m);
	}
}

template <class T>
int Seleccion<T>::pos_min(T* vector, int e, int d){
	int p = e;
	for (int i = e ; i < d; i++){
		if (vector[i] < vector[p])
			p = i;
	}
	return p;
}