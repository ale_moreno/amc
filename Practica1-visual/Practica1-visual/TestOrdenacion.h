#ifndef TESTORDENACION_H
#define TESTORDENACION_H

#include "Ordenacion.h"
#include "Conjunto.h"
#include "Burbuja.h"
#include "Insercion.h"
#include "Shellsort.h"
#include "Mergesort.h"
#include "Quicksort.h"
#include "Seleccion.h"
#include "Heapsort.h"
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <time.h>
#include <fstream>

using namespace std;
template <class T>
struct tOrdenacion
{
    int num_pruebas;
    char nombre[15];
    Ordenacion<T>* estrategia;
    char ecuacion[20];
};

struct timeval{
	int tv_sec;
	int tv_usec;
};

template <class T>
class TestOrdenacion
{
    private:
        vector< tOrdenacion<T> > testMetodos;
        int ini, incr, fin;
        void pintaGrafica(int metodo);
        void pintaGrafica(vector<int> metodos);
    public:
        TestOrdenacion();
        virtual ~TestOrdenacion();
        void comprobarMetodos(int talla);
        void compararMetodos(vector<int> metodos);
};

#include "TestOrdenacion.cpp"

#endif // TESTORDENACION_H
