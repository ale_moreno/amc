
using namespace std;


///////////// Implementación de los métodos /////////////
template <class T>
Conjunto<T>::Conjunto(int n)
{
    datos = new T[n];
    tam = n;
}

template <class T>
Conjunto<T>::~Conjunto()
{
    delete[] datos;
}

template <class T>
void Conjunto<T>::ordena()
{
    int asig = 0,
        cmp = 0;
    for (int i = 0; i<tam-1; i++)
    {
        for (int j= i+1; j<tam; j++)
        {
            if (datos[j] < datos[i])
            {
                int tmp = datos[i];
                datos[i] = datos[j];
                datos[j] = tmp;

                asig += 3;
                cmp++;
            }

            asig += 2;
            cmp++;
        }

        asig += 2;
        cmp++;
    }
    cout<<"La funcion miembro tiene "<<asig<<" asignaciones y "<<cmp<<" comparaciones."<<endl;
}

template <class T>
void Conjunto<T>::escribe ()
{
	for (int i = 0; i < tam; i++)
		cout << datos[i] << (i<tam-1? ", ": "\n");
}

template <class T>
void Conjunto<T>::clonar(const Conjunto<T> &c)
{
    if(datos != NULL)
        delete[] datos;

    tam = c.tam;
    datos = new T[tam];
    for (int i= 0; i<tam; i++)
    {
        datos[i] = c.datos[i];
    }
}

template <>
void Conjunto<int>::generarAlea(/*int talla*/)
{
    /*delete[] datos;
    tam = talla;
    datos = new int[talla];*/
    srand(time(NULL));
    for(int i = 0; i<tam; i++)
    {
        datos[i] = rand()%(tam*10);
    }
}

template <>
void Conjunto<char>::generarAlea(/*int talla*/)
{
    /*delete[] datos;
    tam = talla;
    datos = new int[talla];*/
    srand(time(NULL));
    for(int i = 0; i<tam; i++)
    {
        datos[i] = fmod( rand(), 256 );
    }
}

template <>
void Conjunto<float>::generarAlea(/*int talla*/)
{
    /*delete[] datos;
    tam = talla;
    datos = new int[talla];*/
    srand(time(NULL));
    for(int i = 0; i<tam; i++)
    {
        datos[i] = fmod( rand(),(tam*30/3.1516) );
    }
}

template <>
void Conjunto<double>::generarAlea(/*int talla*/)
{
    /*delete[] datos;
    tam = talla;
    datos = new int[talla];*/
    srand(time(NULL));
    for(int i = 0; i<tam; i++)
    {
        datos[i] = fmod( rand(),(tam*30/3.1516) );
    }
}



template <class T>
void Conjunto<T>::inverso()
{
    //  < Ordenar >
    for (int i = 0; i<tam; i++)
    {
        int temp = datos[i];
        datos[i] = datos[tam - i];
        datos[tam - i] = temp;
    }
}

template <class T>
T* Conjunto<T>::getVector()
{
    return datos;
}

