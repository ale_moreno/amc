#ifndef SELECCION_H
#define SELECCION_H

#include "Ordenacion.h"

using namespace std;

template <class T>
class Seleccion : public Ordenacion<T>
{
public:
	Seleccion();
	~Seleccion();
	void ordena(T* vector, int ini, int fin);
	int pos_min(T* vector, int e, int d);
};

#include "Seleccion.cpp"
#endif