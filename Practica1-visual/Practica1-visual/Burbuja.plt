reset 
set title 'Busqueda KesimoDir Secuencial' 
set key top left vertical inside
set grid
set xlabel 'Talla (n)' 
set ylabel 'Tiempo (milisegundos)' 
y(x) = a*x*log(x)+ b*x + c
fit y(x) 'Burbuja.dat' using 1:2 via a,b,c
plot 'Burbuja.dat' using 1:2 w linespoints title 'Burbuja'
replot y(x) title 'Orden y(x) = a*n*n+b*n+c
set terminal pdf
set output"Burbuja.pdf"  
replot
pause 5 'Pulsa Enter para continuar...'
