

template <class T>
TestOrdenacion<T>::TestOrdenacion():ini(1000),incr(1000),fin(10000)
{

    int repe = 200;
    tOrdenacion<T> bubble,
                inser,
                shell,
                mergeS,
                quick,
				heap,
				selec;

    bubble.num_pruebas = repe;
    inser.num_pruebas = repe;
    shell.num_pruebas = repe;
    mergeS.num_pruebas = repe;
    quick.num_pruebas = repe;

	// Burbuja
    bubble.estrategia = new Burbuja<T>();
    strcpy_s(bubble.nombre,"Burbuja");
    strcpy_s(bubble.ecuacion,"a*n*n+b*n+c");
    testMetodos.push_back(bubble);

    // Inserccion
    inser.estrategia = new Insercion<T>();
    strcpy_s(inser.nombre,"Insercion");
    strcpy_s(inser.ecuacion,"a*n*n+b*n+c");
    testMetodos.push_back(inser);

	// Seleccion
	selec.estrategia = new Seleccion<T>();
	strcpy_s(selec.nombre, "Seleccion");
	strcpy_s(selec.ecuacion, "a*n*n+b*n+c");
	testMetodos.push_back(selec);

    // Shell;
    shell.estrategia = new Shellsort<T>();
	strcpy_s(shell.nombre, "Shellsort");
	strcpy_s(shell.ecuacion, "a*n^(2)+b*n+c");
    testMetodos.push_back(shell);

    // Merge
    mergeS.estrategia = new Mergesort<T>();
	strcpy_s(mergeS.nombre, "Mergesort");
	strcpy_s(mergeS.ecuacion, "a*log(n)+b*n+c");
    testMetodos.push_back(mergeS);

   // Quicksort
    quick.estrategia = new Quicksort<T>();
	strcpy_s(quick.nombre, "Quicksort");
	strcpy_s(quick.ecuacion, "a*log(n)+b*n+c");
    testMetodos.push_back(quick);
	
	// Heapsort
	heap.estrategia = new Heapsort<T>();
	strcpy_s(heap.nombre, "Heapsort");
	strcpy_s(heap.ecuacion, "a*log(n)+b*n+c");
	testMetodos.push_back(heap);
}

//iomanic
template <class T>
TestOrdenacion<T>::~TestOrdenacion()
{
    while( !testMetodos.empty() )
    {
        delete testMetodos.back().estrategia;
        testMetodos.pop_back();
    }
}

template <class T>
void TestOrdenacion<T>::comprobarMetodos(int talla)
{
    Conjunto<T> *vDesorde = new Conjunto<T>(talla);
    Conjunto<T> *copia = new Conjunto<T>(talla);

    vDesorde->generarAlea();
    copia->clonar(*vDesorde);

    for(int i = 0; i<testMetodos.size(); i++)
    {
        cout<<"\nMetodo: "<<testMetodos[i].nombre<<endl;
        cout<<"\tVector sin ordenar: ";
        vDesorde->escribe();
        testMetodos[i].estrategia->ordena( vDesorde->getVector(), 0, talla );
        cout<<"\tVector ordenado: ";
        vDesorde->escribe();
        vDesorde->clonar(*copia);
    }
    delete vDesorde, copia;
}

template <class T>
void TestOrdenacion<T>::compararMetodos(vector<int> metodos)
{
	long double acumulador[7][9];
    char opcG;

    cout<<"Talla ";
    for (int i = 0;i<metodos.size();i++)
        for (int j = 0; j<(fin-ini)/incr; j++)
            acumulador[i][j] = 0;
    for(int i = 0; i<metodos.size(); i++)
    {
      cout<<" \t "<<testMetodos[ metodos[i] ].nombre;
    }
    cout<<endl;
    for(int talla = ini; talla<fin; talla+=incr)
    {
        Conjunto<T> *v= new Conjunto<T>(talla);
        Conjunto<T> *vclon= new Conjunto<T>(talla);
        for (int repe = 0; repe<testMetodos[0].num_pruebas; repe++)
        {
            vclon->generarAlea();
            v->clonar(*vclon);
            for(int i = 0; i<metodos.size(); i++)
            {
				clock_t t_ini, t_fin;
				double secs;

				t_ini = clock();
				testMetodos[metodos[i]].estrategia->ordena(v->getVector(), 0, talla);
				t_fin = clock();

				secs = ((double)(t_fin - t_ini))*1000.0;

				acumulador[i][(talla - ini) / incr] += secs;
				v->clonar(*vclon);
            }

        }
        cout<<"  "<<talla;
        for(int i = 0; i<metodos.size(); i++)
        {

            acumulador[i][(talla-ini)/incr] /= testMetodos[i].num_pruebas;
			cout << " \t  "<< acumulador[i][(talla - ini) / incr];

        }
        cout<<endl;
        delete v, vclon;
    }

        cout<<"Deseas guardar los datos? (y/n): ";
        cin>>opcG;

    if( opcG=='y' || opcG=='Y' )
    {
        ofstream fout;
        for(int i = 0; i<metodos.size(); i++)
        {
            char nombreF[20];
            strcpy_s(nombreF, testMetodos[metodos[i]].nombre);
            strcat_s(nombreF, ".dat");
            fout.open(nombreF);
            for (int j = 0; j<(fin-ini)/incr;j++)
                fout<<j*incr+ini<<"\t"<<acumulador[i][j]<<endl;
            fout.close();
        }
        cout<<"Guardado..."<<endl;
        if(metodos.size()==1)
            pintaGrafica(metodos[0]);
        else
            pintaGrafica(metodos);
    }


}

template <class T>
void TestOrdenacion<T>::pintaGrafica(int metodo)
{
    char opc;
    ofstream fout;
    char nombreF[20];
            strcpy_s(nombreF, testMetodos[metodo].nombre);
			strcat_s(nombreF, ".plt");
    fout.open(nombreF);
    fout<<"reset "<<endl;
    fout<<"set title \'Busqueda KesimoDir Secuencial\' "<<endl;
    fout<<"set key top left vertical inside"<<endl;
    fout<<"set grid"<<endl;
    fout<<"set xlabel \'Talla (n)\' "<<endl;
    fout<<"set ylabel \'Tiempo (milisegundos)\' "<<endl;

    fout<<"y(x) = a*x*log(x)+ b*x + c"<<endl;
    fout<<"fit y(x) \'"<<testMetodos[metodo].nombre<<".dat\' using 1:2 via a,b,c"<<endl;
    fout<<"plot \'"<<testMetodos[metodo].nombre<<".dat\' using 1:2 w linespoints title \'";
        fout<<testMetodos[metodo].nombre<<"\'"<<endl;
    fout<<"replot y(x) title \'Orden y(x) = "<<testMetodos[metodo].ecuacion<<endl;
    fout<<"set terminal pdf"<<endl;
    fout<<"set output\""<<testMetodos[metodo].nombre<<".pdf\"  "<<endl;
    fout<<"replot"<<endl;
    fout<<"pause 5 'Pulsa Enter para continuar...'"<<endl;
    fout.close();

    cout<<"Grafica creada, deseas verla?(y/n): ";
    cin>>opc;

    if (opc=='y' || opc=='Y')
        system(nombreF);
}

template <class T>
void TestOrdenacion<T>::pintaGrafica(vector<int> metodos)
{
    char opc;
    ofstream fout;
    fout.open("Comparacion.plt");
    fout<<"reset "<<endl;
    fout<<"set title \'Comparacion Metodos\' "<<endl;
    fout<<"set key top left vertical inside"<<endl;
    fout<<"set xlabel \'Talla (n)\' "<<endl;
    fout<<"set ylabel \'Tiempo (milisegundos)\' "<<endl;
    fout<<"set grid xtics "<<endl;
    fout<<"set grid ytics "<<endl;
    fout<<"plot \'"<<testMetodos[metodos[0]].nombre<<".dat\' w linespoints title \'";
        fout<<testMetodos[metodos[0]].nombre<<"\' "<<endl;
    for(int i = 1; i<metodos.size(); i++)
    {
        fout<<"replot \'"<<testMetodos[metodos[i]].nombre<<".dat\' w linespoints title \'";
            fout<<testMetodos[metodos[i]].nombre<<"\' "<<endl;
    }

    fout<<"set terminal pdf"<<endl;
    fout<<"set output \"Comparacion.pdf\" "<<endl;
    fout<<"replot"<<endl;
    fout<<"pause 5 \'Pulsa Enter para continuar...\'"<<endl;
    fout.close();

    cout<<"Grafica creada, deseas verla?(y/n): ";
    cin>>opc;

    if (opc=='y' || opc=='Y')
        system("Comparacion.plt");
}
