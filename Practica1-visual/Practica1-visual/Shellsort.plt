reset 
set title 'Busqueda KesimoDir Secuencial' 
set key top left vertical inside
set grid
set xlabel 'Talla (n)' 
set ylabel 'Tiempo (milisegundos)' 
y(x) = a*x*log(x)+ b*x + c
fit y(x) 'Shellsort.dat' using 1:2 via a,b,c
plot 'Shellsort.dat' using 1:2 w linespoints title 'Shellsort'
replot y(x) title 'Orden y(x) = a*n^(2)+b*n+c
set terminal pdf
set output"Shellsort.pdf"  
replot
pause 5 'Pulsa Enter para continuar...'
