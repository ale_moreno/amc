#ifndef BUSQUEDA_H
#define BUSQUEDA_H

using namespace std;
template <class T>
class Busqueda
{
public:
	Busqueda();
	virtual ~Busqueda();
	virtual int busca(T *vec, int tama, int elem) = 0;
};

#endif
