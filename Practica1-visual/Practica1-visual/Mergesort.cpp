

template <class T>
Mergesort<T>::Mergesort(){}

template <class T>
Mergesort<T>::~Mergesort(){}

template <class T>
void Mergesort<T>::ordena(T *vec, int ini, int fin)
{
        mergeS(vec,ini,fin-1);
}

template <class T>
void Mergesort<T>::mergeS(T *vec, int ini, int fin)
{
    if (ini < fin )
    {
        int mitad = (ini+fin)/2;
        mergeS(vec,ini,mitad);
        mergeS(vec, mitad+1, fin);

        mezcla(vec, ini, mitad, fin);
    }
}

template <class T>
void Mergesort<T>::mezcla(T *vec, int ini, int corte, int fin)
{
    int i = ini,
        j = corte+1,
        k = 0,
		numEle = fin-ini+1; //90001

    //T vecOrdenado[numEle];
	T vecOrdenado[90001];

    while (i<=corte && j<=fin)
    {
        if(vec[i]<=vec[j])
        {
            vecOrdenado[k] = vec[i];
            i++;
        }
        else
        {
            vecOrdenado[k] = vec[j];
            j++;
        }
        k++;
    }

    while(i<=corte)
        vecOrdenado[k++] = vec[i++];

    while(j<=fin)
        vecOrdenado[k++] = vec[j++];

    for(int x = 0; x<numEle; x++)
    {
        vec[ini+x] = vecOrdenado[x];
    }
}
