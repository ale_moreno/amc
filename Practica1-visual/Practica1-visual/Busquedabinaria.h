#ifndef BBINARIA_H
#define BBINARIA_H


#include "Busquedas.h"
#include "Quicksort.h"

using namespace std;
template <class T>
class Busbinaria:public Busqueda<T>
{
private:
	int busca(T *vec, int elem, int ini, int fin);
public:
	Busbinaria();
	~Busbinaria();
	int busca(T* vec, int tama ,int elem);
};

#include "Busquedabinaria.cpp"

#endif